<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi;

//
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Tiat\Collection\MessageBus\MessageBusInterface;

/**
 * Base layer methods
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiHelperInterface extends LoggerAwareInterface {
	
	/**
	 * Get PSR Log (interface) from the Layer
	 *
	 * @return null|LoggerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getLog() : ?LoggerInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkLog() : bool;
	
	/**
	 * Set PSR Log (interface) for the Layer
	 *
	 * @param    LoggerInterface    $logger
	 *
	 * @return AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setLog(LoggerInterface $logger) : AsiHelperInterface;
	
	/**
	 * @return AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetLog() : AsiHelperInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function checkMessageBus() : bool;
	
	/**
	 * @return null|MessageBusInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessageBus() : ?MessageBusInterface;
	
	/**
	 * @param    MessageBusInterface    $messageBus
	 *
	 * @return AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessageBus(MessageBusInterface $messageBus) : AsiHelperInterface;
	
	/**
	 * @return AsiHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMessageBus() : AsiHelperInterface;
}
