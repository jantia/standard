<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi;

//
use Tiat\Standard\Pipeline\PipelineInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiPipelineHelperInterface {
	
	/**
	 * Get the pipeline interface object
	 *
	 * @return null|PipelineInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipeline() : ?PipelineInterface;
	
	/**
	 * Reset the used pipeline
	 *
	 * @return AsiPipelineHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetPipeline() : AsiPipelineHelperInterface;
	
	/**
	 * Set new pipeline interface object
	 *
	 * @param    PipelineInterface|string    $pipeline
	 *
	 * @return AsiPipelineHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipeline(PipelineInterface|string $pipeline) : AsiPipelineHelperInterface;
	
	/**
	 * Use the pipeline
	 *
	 * @param    callable|object    $process
	 * @param    array              $options
	 * @param    string             $method
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function usePipeline(callable|object $process, array $options, string $method) : bool;
	
	/**
	 * Get default pipeline interface name
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getPipelineInterfaceDefault() : string;
	
	/**
	 * Set default pipeline interface
	 *
	 * @param    string    $interface
	 *
	 * @return AsiPipelineHelperInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPipelineInterfaceDefault(string $interface) : AsiPipelineHelperInterface;
}
