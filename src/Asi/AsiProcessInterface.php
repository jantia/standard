<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi;

//
use Jantia\Asi\Register\AsiRegisterInterface;
use Psr\Log\LogLevel;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Monitor\MonitorInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiProcessInterface extends AsiHelperInterface, AsiPipelineHelperInterface, AsiRegisterInterface, ClassLoaderInterface {
	
	/**
	 * Execute commands before the object will be destroyed.
	 *
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void;
	
	/**
	 * Output code as HTTP response code between 100 and 599 (or false if failure)
	 *
	 * @return int|false
	 * @since   3.0.0 First time introduced.
	 */
	public function getOutputCode() : int|false;
	
	/**
	 * Output code as HTTP response code between 100 and 599.
	 *
	 * @param    int    $code
	 *
	 * @return AsiProcessInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOutputCode(int $code) : AsiProcessInterface;
	
	/**
	 * Return output as LogLevel.
	 *
	 * @return LogLevel
	 * @since   3.0.0 First time introduced.
	 */
	public function getOutputLevel() : LogLevel;
	
	/**
	 * Set output as LogLevel.
	 *
	 * @param    LogLevel    $level
	 *
	 * @return AsiProcessInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setOutputLevel(LogLevel $level) : AsiProcessInterface;
	
	/**
	 * Return output error as string.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function outputError() : ?string;
	
	/**
	 * Return output result as string.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function outputResult() : ?string;
	
	/**
	 * Throw error if the condition is greater than.
	 *
	 * @param    int    $condition
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function throwErrorIf(int $condition) : void;
	
	/**
	 * Set the timeout as seconds for the process.
	 *
	 * @param    int    $timeout
	 *
	 * @return AsiProcessInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function timeout(int $timeout) : AsiProcessInterface;
	
	/**
	 * @return null|MonitorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getMonitorInterface() : ?MonitorInterface;
	
	/**
	 * @param    MonitorInterface|NULL    $monitor
	 *
	 * @return AsiProcessInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMonitorInterface(MonitorInterface $monitor = NULL) : AsiProcessInterface;
	
	/**
	 * @return AsiProcessInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMonitorInterface() : AsiProcessInterface;
}
