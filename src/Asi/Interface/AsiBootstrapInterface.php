<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi\Interface;

//
use Jantia\Standard\Asi\AsiProcessInterface;
use Psr\Log\LoggerInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiBootstrapInterface extends AsiProcessInterface {
	
	/**
	 * Register custom error handler. In this case the default is Jantia/Logit
	 * If you like to use other custom error handler then override this method
	 *
	 * @param    LoggerInterface    $logger
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function registerErrorHandler(LoggerInterface $logger) : void;
}
