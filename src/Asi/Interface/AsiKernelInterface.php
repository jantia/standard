<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi\Interface;

//
use Jantia\Standard\Asi\AsiProcessInterface;
use Psr\Http\Message\RequestInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiKernelInterface extends AsiProcessInterface {
	
	/**
	 * @return null|RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestInterface() : ?RequestInterface;
	
	/**
	 * @param    RequestInterface    $request
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequestInterface(RequestInterface $request) : void;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRequestInterface() : void;
}
