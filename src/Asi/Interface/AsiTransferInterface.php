<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi\Interface;

//
use Jantia\Standard\Asi\AsiProcessInterface;
use Tiat\Standard\Request\RequestElement;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiTransferInterface extends AsiProcessInterface {
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUrl() : ?string;
	
	/**
	 * @param    string    $url
	 *
	 * @return AsiTransferInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setUrl(string $url) : AsiTransferInterface;
	
	/**
	 * @return AsiTransferInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetUrl() : AsiTransferInterface;
	
	/**
	 * @param ...$args
	 *
	 * @return AsiTransferInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouter(...$args) : AsiTransferInterface;
	
	/**
	 * @param    RequestElement|NULL    $key
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestElement(RequestElement $key = NULL) : mixed;
}
