<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi\Layer\Kernel;

//
use Jantia\Asi\Interface\Kernel\Layer\Datalink\AsiKernelDatalink;
use Jantia\Standard\Asi\AsiProcessInterface;
use Laminas\Validator\ValidatorInterface;
use Tiat\Standard\DataModel\VariableElement;
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * Datalink layer will handle user input to Jantia ASI standard.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiKernelDatalinkInterface extends AsiProcessInterface, ParametersPluginInterface {
	
	/**
	 * Register variables from GET/POST/FILES/COOKIE/ENV/SERVER
	 * Use first letter from each pre-defined variables or single word
	 * Order is always from left to right
	 *
	 * @param    VariableElement|string    $order
	 * @param    bool                      $override
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function registerVariables(VariableElement|string $order, bool $override) : AsiKernelDatalinkInterface;
	
	/**
	 * Unregister variables from GET/POST/FILES/COOKIE/ENV/SERVER
	 * Use first letter from each pre-defined variables or single word
	 * Order is always from left to right
	 *
	 * @param    string|VariableElement    $order
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function unregisterVariables(string|VariableElement $order) : AsiKernelDatalinkInterface;
	
	/**
	 * Get all registered and validated user input variables from GET/POST/FILES/COOKIE/ENV/SERVER
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getValidatedVariables() : ?array;
	
	/**
	 * Set validators from an array en masse. Array must have a key (HTTP method) OR unique variable name and the validators in array.
	 * The array must include string or object(s) which will implemented to Stdlib ValidatorInterface
	 *
	 * @param    array    $validators
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setValidators(array $validators) : AsiKernelDatalinkInterface;
	
	/**
	 * Set validator for variable validating. Key is the HTTP method (GET, POST...) OR unique variable name.
	 *
	 * @param    string                      $key
	 * @param    ValidatorInterface|array    $validator
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setValidator(string $key, ValidatorInterface|array $validator) : AsiKernelDatalinkInterface;
	
	/**
	 * Return array with validators or null
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getValidators() : array|null;
	
	/**
	 * Return validator with $key. Key is the HTTP method (GET, POST...) OR unique variable name.
	 *
	 * @param    string    $key
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getValidator(string $key) : array|null;
	
	/**
	 * Delete given validator. Key is the HTTP method (GET, POST...) OR unique variable name.
	 *
	 * @param    string    $key
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function deleteValidator(string $key) : AsiKernelDatalinkInterface;
	
	/**
	 * Reset all validators.
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetValidators() : AsiKernelDatalinkInterface;
	
	/**
	 * Is variable validating case-sensitive or not
	 *
	 * @param    bool    $caseSensitive
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCaseSensitive(bool $caseSensitive) : AsiKernelDatalinkInterface;
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isCaseSensitive() : bool;
	
	/**
	 * Get all user input
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getUserInput() : array;
	
	/**
	 * If method is not given then all vars will be destroyed.
	 *
	 * @param    VariableElement|string|NULL    $method
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetUserInput(VariableElement|string $method = NULL) : AsiKernelDatalinkInterface;
	
	/**
	 * Get the current register order for user input (variables).
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getVariablesOrder() : string;
	
	/**
	 * Set custom user input register order. if $register is TRUE then trigger will register all variables again.
	 *
	 * @param    string    $order
	 * @param    bool      $register
	 *
	 * @return AsiKernelDatalink
	 * @since   3.0.0 First time introduced.
	 */
	public function setVariablesOrder(string $order, bool $register) : AsiKernelDatalinkInterface;
	
	/**
	 * Reset user input register order to default.
	 *
	 * @return AsiKernelDatalinkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetVariablesOrder() : AsiKernelDatalinkInterface;
	
	/**
	 * Validate user input based to current register order.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function validateUserInput() : bool;
}
