<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi\Layer\Kernel;

//
use Jantia\Standard\Asi\AsiProcessInterface;
use Tiat\Standard\Request\RequestElement;
use Tiat\Standard\Request\RequestInterface;
use WeakMap;

/**
 * Network layer will handle data from http/other server to Jantia ASI standard.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiKernelNetworkInterface extends AsiProcessInterface {
	
	/**
	 * Get request as WeakMap array (if $key is null) or string|int (key of array):
	 * - path => string
	 * - query => array
	 * - host => string
	 * - scheme => string
	 * - port => int
	 * - userinfo => string
	 *
	 * @param    RequestElement|NULL    $key
	 *
	 * @return null|WeakMap|array|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestElement(RequestElement $key = NULL) : WeakMap|array|string|null;
	
	/**
	 * Set detected HTTP method from server.
	 *
	 * @param    string    $method
	 *
	 * @return AsiKernelNetworkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMethod(string $method) : AsiKernelNetworkInterface;
	
	/**
	 * Get detected HTTP method. Support for HttpMethodCustom. Default is GET
	 *
	 * @return string
	 * @see     HttpMethodCustom
	 * @since   3.0.0 First time introduced.
	 */
	public function getMethod() : string;
	
	/**
	 * @return null|RequestInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequestInterface() : ?RequestInterface;
	
	/**
	 * @param    RequestInterface    $request
	 *
	 * @return AsiKernelNetworkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequestInterface(RequestInterface $request) : AsiKernelNetworkInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getUrl() : ?string;
	
	/**
	 * @param    string    $url
	 *
	 * @return AsiKernelNetworkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setUrl(string $url) : AsiKernelNetworkInterface;
	
	/**
	 * @return AsiKernelNetworkInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetUrl() : AsiKernelNetworkInterface;
}
