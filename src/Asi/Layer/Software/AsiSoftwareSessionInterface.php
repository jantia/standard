<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Asi\Layer\Software;

//
use Jantia\Standard\Asi\AsiProcessInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface AsiSoftwareSessionInterface extends AsiProcessInterface {

}
