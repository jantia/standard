<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Message;

/**
 * This class is for collection class internal messages with Psr\Log\LoggerInterface levels. Levels are collected
 * from $_msgTemplates array keys and message is used as index for translation $_msgTemplates['templates'][$level][$key].
 * Example to init templates:
 * protected array $_msgTemplates = [LogLevel::WARNING => [self::INVALID_KEY => "My translated error message."]
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/jantia/standard
 */
interface MessageInterface {
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void;
	
	/**
	 * Set message
	 *
	 * @param    null|string          $key
	 * @param    null|string|array    $message
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessage(?string $key, string|array|null $message) : void;
	
	/**
	 * Sets message templates given as an array, where the array keys are the message keys,
	 *  and the array values are the message template strings.
	 *
	 * @param    array    $messages
	 *
	 * @return MessageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setMessages(array $messages) : MessageInterface;
	
	/**
	 * Returns array of messages. The array keys are message identifiers,
	 * and the array values are the corresponding human-readable message strings.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getMessages() : array;
	
	/**
	 * Find the PSG\LogLevel level from internal messages array with message key
	 *
	 * @param    string    $key
	 *
	 * @return string|false
	 * @since   3.0.0 First time introduced.
	 */
	public function findMessageLevel(string $key) : string|false;
	
	/**
	 * Reset messages
	 *
	 * @return MessageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMessages() : MessageInterface;
	
	/**
	 * Reset message(s) on group level or single message from LogLevel group
	 *
	 * @param    string|int         $key      LogLevel
	 * @param    string|int|NULL    $index    Custom key
	 *
	 * @return MessageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetMessage(string|int $key, string|int $index = NULL) : MessageInterface;
	
	/**
	 * Count messages in backend and group them with LogLevel levels
	 *
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function countMessages() : int;
}
