<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Platform;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * ENV values for Tiat Framework which can/should exist if you like to use it as a Platform.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum PlatformEnv: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_APP_NAME = 'APP_NAME';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_APPS_DEBUG = 'APPS_DEBUG';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_APPS_ROOT_DIR = 'APPS_ROOT_DIR';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_APPS_RUNMODE = 'APPS_RUNMODE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_LOG_DEFAULT = 'LOG_DEFAULT';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_LOG_ERROR = 'LOG_ERROR';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_LOG_LEVEL = 'LOG_LEVEL';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_NAMESPACE = 'NAMESPACE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENV_PLATFORM_DEFAULT = 'PLATFORM_DEFAULT';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TRACE_ID = 'TRACE_ID';
}
