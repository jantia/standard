<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Standard
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Standard\Version;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/jantia/standard
 */
interface VersionPackageInterface {
	
	/**
	 * Get the official package name.
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getPackageName() : ?string;
	
	/**
	 * Set the package name. If name is null then use the composer resolve it through resolvePackageName().
	 *
	 * @param    null|string    $name
	 *
	 * @return VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setPackageName(string $name = NULL) : VersionPackageInterface;
	
	/**
	 * Resolve package name from namespace if $name not defined.
	 *
	 * @param    null|string    $name
	 *
	 * @return VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resolvePackageName(string $name = NULL) : VersionPackageInterface;
	
	/**
	 * Get installed package official version tag info (autodetect if using composer, otherwise set by self).
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getVersionInfo() : ?string;
	
	/**
	 * Official installed version tag (autodetect if using composer, otherwise set by self).
	 *
	 * @param    string    $version
	 *
	 * @return VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setVersionInfo(string $version) : VersionPackageInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * @param    string    $name
	 *
	 * @return VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : VersionPackageInterface;
	
	/**
	 * Get package result as array.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : array;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function __toString() : string;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function toString() : string;
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function toArray() : array;
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function __toArray() : array;
}
